public class Local {
    private String nome;
    private String telefone;
    private String ruaNum;
    private String bairro;
    private String cep;
    private String pontoReferencia;
    private int distanciaSindicato;

    public Local() { }

    public Local(String nome, String telefone, String ruaNum, String bairro, String cep, String pontoReferencia, int distanciaSindicato) {
        this.nome = nome;
        this.telefone = telefone;
        this.ruaNum = ruaNum;
        this.bairro = bairro;
        this.cep = cep;
        this.pontoReferencia = pontoReferencia;
        this.distanciaSindicato = distanciaSindicato;
    }

    public String getNome() {
        return nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getRuaNum() {
        return ruaNum;
    }

    public String getBairro() {
        return bairro;
    }

    public String getCep() {
        return cep;
    }

    public String getPontoReferencia() {
        return pontoReferencia;
    }

    public int getDistanciaSindicato() {
        return distanciaSindicato;
    }

    @Override
    public String toString() {
        return nome;
    }
}
