import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

import java.awt.*;

public class ModelDAO<T> {
    MongoCollection<Document> pCollection;

    public ModelDAO(Class<T> tClass)
    {
        pCollection = MongoDBHandler.getCollection(tClass);
    }

    public void Insert(T prof){
        pCollection.insertOne(MongoDBHandler.ExtractDocument(prof));
    }

    public List<T> Read(BasicDBObject params, Class<T> tClass)
    {
        List<T> result = new ArrayList<T>();
        MongoCursor iterator = pCollection.find(params).iterator();
        while (iterator.hasNext())
        {
            result.add(MongoDBHandler.ExtractClass((Document)(iterator.next()), tClass));
        }
        return result;
    }
    public List<T> Read(Class<T> tClass)
    {
        List<T> result = new ArrayList<T>();
        MongoCursor iterator = pCollection.find().iterator();
        while (iterator.hasNext())
        {
            result.add(MongoDBHandler.ExtractClass((Document)(iterator.next()), tClass));
        }
        return result;
    }

    public void Update(BasicDBObject params, T prof) { pCollection.updateMany(params, new Document("$set", MongoDBHandler.ExtractDocument(prof))); }
}
