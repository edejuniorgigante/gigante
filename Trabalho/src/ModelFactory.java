public interface ModelFactory<T> {
    T BuildItem();
}
