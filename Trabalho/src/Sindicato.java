
public class Sindicato {
    private String nome;
    @DataField(myType = DataType.CLASS, classType = Coordenador.class)
    private Coordenador coordenador;

    public Sindicato(){ }

    public Sindicato(String nome, Coordenador coordenador) {
        this.nome = nome;
        this.coordenador = coordenador;
    }

    public String getNome() {
        return nome;
    }

    public Coordenador getCoordenador() {
        return coordenador;
    }
}
