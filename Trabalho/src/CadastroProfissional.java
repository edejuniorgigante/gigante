import com.mongodb.BasicDBObject;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class CadastroProfissional {
    public JPanel telaCadastroInstrutor;
    private JButton cadastrarProfissionalButton;
    private JTextField nomeInstrutor;
    private JTextField rgInstrutor;
    private JTextField cpfInstrutor;
    private JTextField pisInstrutor;
    private JTextField dnInstrutor;
    private JTextField rnInstrutor;
    private JTextField bairroInstrutor;
    private JTextField cepInstrutor;
    private JTextField cidadeInstrutor;
    private JComboBox tipoProfissional;
    private JComboBox listaProfissional;

    private boolean edit = false;

    public CadastroProfissional()
    {
        for (TipoProfissional item : TipoProfissional.values()) {
            tipoProfissional.addItem(item.toString());
        }

        ModelDAO<Profissional> profissionalModelDAO = new ModelDAO<>(Profissional.class);
        List<Profissional> profissionais = profissionalModelDAO.Read(Profissional.class);

        listaProfissional.addItem("Novo Profissional...");
        for (Profissional profissional: profissionais) {
            listaProfissional.addItem(profissional.getNome());
        }

        listaProfissional.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Profissional>profissionais = profissionalModelDAO.Read(new BasicDBObject("nome", listaProfissional.getSelectedItem().toString()),Profissional.class);
                if (profissionais.size() != 0){
                    Profissional atualProfissional = profissionais.get(0);
                    nomeInstrutor.setText(atualProfissional.getNome());
                    rgInstrutor.setText(atualProfissional.getRg());
                    cpfInstrutor.setText(atualProfissional.getCpf());
                    pisInstrutor.setText(atualProfissional.getPis());
                    dnInstrutor.setText(atualProfissional.getDataNascimento());
                    rnInstrutor.setText(atualProfissional.getRuaNum());
                    bairroInstrutor.setText(atualProfissional.getBairro());
                    cepInstrutor.setText(atualProfissional.getCep());
                    cidadeInstrutor.setText(atualProfissional.getCidade());
                    tipoProfissional.setSelectedItem(atualProfissional.getTipo().toString());
                    edit = true;
                }
                else
                {
                    Profissional atualProfissional = profissionais.get(0);
                    nomeInstrutor.setText("");
                    rgInstrutor.setText("");
                    cpfInstrutor.setText("");
                    pisInstrutor.setText("");
                    dnInstrutor.setText("");
                    rnInstrutor.setText("");
                    bairroInstrutor.setText("");
                    cepInstrutor.setText("");
                    cidadeInstrutor.setText("");
                    edit = false;
                }
            }
        });

        cadastrarProfissionalButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                if (!edit) {
                    profissionalModelDAO.Insert(new Profissional((TipoProfissional) Enum.valueOf(TipoProfissional.class, tipoProfissional.getSelectedItem().toString()),
                            nomeInstrutor.getText(),
                            rgInstrutor.getText(),
                            cpfInstrutor.getText(),
                            pisInstrutor.getText(),
                            dnInstrutor.getText(),
                            rnInstrutor.getText(),
                            bairroInstrutor.getText(),
                            cepInstrutor.getText(),
                            cidadeInstrutor.getText()));
                }
                else {
                    profissionalModelDAO.Update(new BasicDBObject("nome", listaProfissional.getSelectedItem().toString()),
                                                new Profissional((TipoProfissional) Enum.valueOf(TipoProfissional.class, tipoProfissional.getSelectedItem().toString()),
                                                        nomeInstrutor.getText(),
                                                        rgInstrutor.getText(),
                                                        cpfInstrutor.getText(),
                                                        pisInstrutor.getText(),
                                                        dnInstrutor.getText(),
                                                        rnInstrutor.getText(),
                                                        bairroInstrutor.getText(),
                                                        cepInstrutor.getText(),
                                                        cidadeInstrutor.getText()));
                }
            }
        });
    }
}
