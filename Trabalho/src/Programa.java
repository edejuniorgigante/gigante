import java.util.List;

public class Programa extends Projeto{
    private int qntModulos;
    private int cargaTotal;
    private List<Curso> modulos;

    public Programa(int numProj, String nome, Municipio municipio, int minParticipantes, int maxParticipantes,
                    List<Profissional> profissionais, int id1, int qntModulos, int cargaTotal, List<Curso> modulos) {
        super(numProj, nome, municipio, minParticipantes, maxParticipantes, profissionais);
        this.qntModulos = qntModulos;
        this.cargaTotal = cargaTotal;
        this.modulos = modulos;
    }
}
