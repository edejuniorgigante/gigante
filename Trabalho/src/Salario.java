public class Salario {
    @DataField(myType = DataType.ENUM, classType = TipoProfissional.class)
    private TipoProfissional tipoProfissional;
    private double valor;

    public Salario() {

    }

    public Salario(TipoProfissional tipoProfissional, double valor) {
        this.tipoProfissional = tipoProfissional;
        this.valor = valor;
    }

    public TipoProfissional getTipoProfissional() {
        return tipoProfissional;
    }

    public double getValor() {
        return valor;
    }
}
