public class Coordenador {
    private String nome;
    private String cpf;
    private String rg;
    private String telefonde;

    public Coordenador() { }

    public Coordenador(int id, String nome, String cpf, String rg, String telefonde) {
        this.nome = nome;
        this.cpf = cpf;
        this.rg = rg;
        this.telefonde = telefonde;
    }

    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public String getRg() {
        return rg;
    }

    public String getTelefonde() {
        return telefonde;
    }
}
