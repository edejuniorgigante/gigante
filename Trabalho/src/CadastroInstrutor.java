import javax.swing.*;

public class CadastroInstrutor {
    private JPanel telaCadastroInstrutor;
    private JButton cadastrarInstrutorButton;
    private JTextField nomeInstrutor;
    private JTextField rgInstrutor;
    private JTextField cpfInstrutor;
    private JTextField pisInstrutor;
    private JTextField dnInstrutor;
    private JTextField rnInstrutor;
    private JTextField bairroInstrutor;
    private JTextField cepInstrutor;
    private JTextField cidadeInstrutor;
    private JTextField rsInstrutor;
}
