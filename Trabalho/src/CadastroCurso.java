import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class CadastroCurso {
    public JPanel cadastrarCurso;
    private JPanel telaCurso;
    private JButton cadastarCursoButton;
    private JTextField maxParticipantes;
    private JTextField nomeCurso;
    private JTextField codigoCurso;
    private JTextField qntIntrutoresCurso;
    private JTextField nomeInstrutor;
    private JTextField minParticipantes;
    private JTextField valorCoordenador;
    private JTextField cargaHoraria;
    private JComboBox listarLocais;
    private JComboBox listarMunicipio;
    private JComboBox listarCoordenador;


    public CadastroCurso() {

        ModelDAO<Municipio> municipioModelDAO = new ModelDAO<>(Municipio.class);
        List<Municipio> municipios = municipioModelDAO.Read(Municipio.class);

        for (Municipio municipio:municipios) {
            listarMunicipio.addItem(municipio);
        }

        listarLocais.removeAllItems();
        for (Local local: ((Municipio) listarMunicipio.getSelectedItem()).getLocais()) {
            listarLocais.addItem(local);
        }

        cadastarCursoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });


        listarMunicipio.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                listarLocais.removeAllItems();
                for (Local local: ((Municipio) listarMunicipio.getSelectedItem()).getLocais()) {
                    listarLocais.addItem(local);
                }
            }
        });
    }
}
