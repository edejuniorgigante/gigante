import com.mongodb.*;
import com.mongodb.client.*;
import org.bson.Document;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        MongoClient mongo = new MongoClient("localhost", 27017);

        MongoDatabase database = mongo.getDatabase("gigante");


        for (MongoCursor<String> i = database.listCollectionNames().iterator(); i.hasNext();) {
            System.out.println(i.next());
        }

        MongoCollection<Document> teste = database.getCollection("Profissional");

        Profissional profissional = new Profissional(TipoProfissional.InstrutorSuperior, "Sandra de Lima Gigante", "401.900.598-63", "49.001.665-0", "123.123.123-12",
                new Date("1993/08/17"), "Rua Herminia 752", "Centro", "Jalão", "15706-260");



        ModelDAO<Municipio> municipioModelDAO = new ModelDAO<>(Municipio.class);

        Municipio municipio = new Municipio(0, "Jalão", 3);

        municipioModelDAO.Insert(municipio);

//        ModelDAO<Profissional> modelDAO = new ModelDAO<>(Profissional.class);
//        List<Profissional> prof = modelDAO.Read(new BasicDBObject("bairro", "Bosta"), new ModelFactory<Profissional>() {
//            @Override
//            public Profissional BuildItem() {
//                return new Profissional();
//            }
//        });
//
//        System.out.println(prof.size());

//        cursor = teste.find(new BasicDBObject("bairro", "Centro"));
//
//        System.out.println(cursor);

    }
}
