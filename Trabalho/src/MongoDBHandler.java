import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import javax.print.Doc;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class MongoDBHandler {
    static MongoClient mongo = new MongoClient("localhost", 27017);
    static MongoDatabase database = mongo.getDatabase("gigante");

    static MongoCollection<Document> getCollection(Class tClass)
    {
        return database.getCollection(tClass.getName());
    }

    public static <T> Document ExtractDocument(T obj)
    {
        Document documentResult = new Document();
        Field[] fields = obj.getClass().getDeclaredFields();

        for (Field field : fields) {
            try {
                field.setAccessible(true);
                Annotation annotation = field.getDeclaredAnnotation(DataListField.class);
                if (annotation == null) {
                    annotation = field.getDeclaredAnnotation(DataField.class);
                }

                if (annotation != null)
                {
                    if (annotation.annotationType() == DataListField.class)
                    {
                        List<Object> listField = (List<Object>)field.get(obj);
                        BasicDBList itens = new BasicDBList();
                        switch (((DataListField)annotation).myType())
                        {
                            case ENUM:
                                for (Object iten : listField)
                                {
                                    itens.add(iten.toString());
                                }
                                break;

                            case CLASS:
                                for (Object iten : listField)
                                {
                                    itens.add(ExtractDocument(iten));
                                }
                                break;

                            case NONE:
                                for (Object iten : listField)
                                {
                                    itens.add(iten);
                                }
                                break;
                        }
                        documentResult.append(field.getName(), itens);
                    }
                    else if(annotation.annotationType() == DataField.class)
                    {
                        switch (((DataField)annotation).myType())
                        {
                            case ENUM:
                                documentResult.append(field.getName(), field.get(obj).toString());
                                break;

                            case CLASS:
                                documentResult.append(field.getName(), ExtractDocument(field.get(obj)));
                                break;
                        }
                    }
                }
                else
                {
                    documentResult.append(field.getName(), field.get(obj));
                }
            }
            catch (Exception e)
            {
                System.out.println(e.getMessage());
            }
        }

        return  documentResult;
    }

    public static <T> T ExtractClass(Document doc, Class<T> obj)
    {
        Field[] fields = obj.getDeclaredFields();

        try
        {
            T result = obj.newInstance();

            for (Field field : fields) {
                try {
                    field.setAccessible(true);
                    Annotation annotation = field.getDeclaredAnnotation(DataListField.class);
                    if (annotation == null) {
                        annotation = field.getDeclaredAnnotation(DataField.class);
                    }
                    if (annotation != null) {

                        if (annotation.annotationType() == DataListField.class) {
                            List listField = new ArrayList();
                            ArrayList itens = (ArrayList) doc.get(field.getName());
                            switch (((DataListField) annotation).myType()) {
                                case ENUM:
                                    for (Object iten : itens) {
                                        listField.add(Enum.valueOf(((DataListField) annotation).classType(), iten.toString()));
                                    }
                                    break;

                                case CLASS:
                                    for (Object iten : itens) {
                                        listField.add(ExtractClass((Document) iten, ((DataListField) annotation).classType()));
                                    }
                                    break;

                                case NONE:
                                    for (Object iten : itens) {
                                        listField.add(iten);
                                    }
                                    break;
                            }
                            field.set(result, listField);
                        } else if (annotation.annotationType() == DataField.class) {
                            switch (((DataField) annotation).myType()) {
                                case ENUM:
                                    field.set(result, Enum.valueOf(((DataField) annotation).classType(), doc.getString(field.getName())));
                                    break;

                                case CLASS:
                                    field.set(result, ExtractClass((Document) (doc.get(field.getName())), ((DataField) annotation).classType()));
                                    break;
                            }
                        }
                    }
                    else
                    {
                        field.set(result, doc.get(field.getName()));
                    }
                }
                catch (Exception e)
                {
                    System.out.println(e.getMessage());
                }
            }

            return result;
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }
    }
}
