import java.util.Date;
import java.util.List;

public class Curso extends Projeto {
    private double cargaTotal;
    private double cargaDiaria;
    private List<String> datas;
    private  List<Pendencia> preRealizacao;
    private  List<Pendencia> emRealizacao;

    public Curso(){ }

    public Curso(int numProj, String nome, Municipio municipio, int minParticipantes, int maxParticipantes,
                 List<Profissional> profissionais, int id1, double cargaTotal, double cargaDiaria, List<String> datas,
                 List<Pendencia> preRealizacao, List<Pendencia> emRealizacao) {
        super(numProj, nome, municipio, minParticipantes, maxParticipantes, profissionais);
        this.cargaTotal = cargaTotal;
        this.cargaDiaria = cargaDiaria;
        this.datas = datas;
        this.preRealizacao = preRealizacao;
        this.emRealizacao = emRealizacao;
    }
}
