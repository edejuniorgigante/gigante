import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;


public class inicio {
    public JPanel back;
    private JPanel emAndamento;
    private JPanel emAtraso;
    private JPanel emEspera;
    private JButton botaoCurso;
    private JButton botaoProfissional;
    private JButton inicio;
    private JButton botaoMunicipio;
    private JButton botaoSindicato;
    private JButton botaoSalario;
    private JButton pendênciasButton;
    private JButton botaoPrograma;
    private JList listaCurso;
    private JMenuBar menuPrincipal;

    static JFrame frame = new JFrame("inicio");

    DefaultListModel<Curso> model = new DefaultListModel<>();

    public static void main(String[] args) {

        frame.setContentPane(new inicio().back);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public inicio() {

        ModelDAO<Curso> cursoModelDAO = new ModelDAO<>(Curso.class);

        List<Curso> cursos = cursoModelDAO.Read(Curso.class);

        System.out.println(cursos.size());

        listaCurso.removeAll();

        for (Curso curso : cursos) {
            model.add(0, curso);
        }
        listaCurso.setModel(model);


        //Projeto
        botaoCurso.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog dialog = new JDialog(frame, "Cadastro Curso");
                dialog.setContentPane(new CadastroCurso().cadastrarCurso);
                dialog.pack();
                dialog.setVisible(true);
            }
        });



        //Salario
        botaoSalario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog dialog = new JDialog(frame, "Cadastro Salário");
                dialog.setContentPane(new CadastroSalario().panel1);
                dialog.pack();
                dialog.setVisible(true);
            }
        });

        //Profissional
        botaoProfissional.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog dialog = new JDialog(frame, "Cadastro Profissional");
                dialog.setContentPane(new CadastroProfissional().telaCadastroInstrutor);
                dialog.pack();
                dialog.setVisible(true);
            }
        });

        //Município
        botaoMunicipio.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog dialog = new JDialog(frame, "Cadastro Município");
                dialog.setContentPane(new CadastroMunicipio(dialog).telaMunicipio);
                dialog.pack();
                dialog.setVisible(true);
            }
        });

        //Programa
        botaoPrograma.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog dialog = new JDialog(frame, "Cadastro Programa");
                dialog.setContentPane(new CadastroPrograma().cadastrarPrograma);
                dialog.pack();
                dialog.setVisible(true);
            }
        });


        //Sindicato
        botaoSindicato.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog dialog = new JDialog(frame, "Cadastro Sindicato");
                dialog.setContentPane(new CadastroSindicato().panel1);
                dialog.pack();
                dialog.setVisible(true);
            }
        });

        //Pendencia
        pendênciasButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog dialog = new JDialog(frame, "Cadastro Pendencia");
                dialog.setContentPane(new CadastroPendencia().telaPendencia);
                dialog.pack();
                dialog.setVisible(true);
            }
        });
    }
}
