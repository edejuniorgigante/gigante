import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CadastroLocal {
    public JPanel telaLocal;
    private JButton cadastrarLocalButton;
    private JTextField nomeLocal;
    private JTextField telLocal;
    private JTextField rnLocal;
    private JTextField bairroLocal;
    private JTextField cepLocal;
    private JTextField refLocal;
    private JTextField distLocal;

    public CadastroLocal(JDialog myFrame, CallBackEvent<Local> callBackEvent) {
        cadastrarLocalButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Local local = new Local(nomeLocal.getText(), telLocal.getText(), rnLocal.getText(), bairroLocal.getText(),
                        cepLocal.getText(), refLocal.getText(), Integer.decode(distLocal.getText()));
                callBackEvent.CallBack(local);
                myFrame.setVisible(false);
            }
        });
    }
}
