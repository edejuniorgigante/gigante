import com.mongodb.BasicDBObject;
import sun.security.x509.EDIPartyName;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class CadastroMunicipio {
    public JPanel telaMunicipio;
    private JComboBox comboBox1;
    private JTextField nomeMunicipio;
    private JTextField issMunicipio;
    private JList list1;
    private JButton cadastrarMunicípioButton;
    private JButton adicionarButton;
    private JButton removerButton;

    private boolean edit = false;

    DefaultListModel<Local> model = new DefaultListModel<>();

    public CadastroMunicipio(JDialog myFrame) {

        ModelDAO<Municipio> municipioModelDAO = new ModelDAO<>(Municipio.class);
        List<Municipio> municipios = municipioModelDAO.Read(Municipio.class);

        comboBox1.addItem("Novo município...");
        for (Municipio municipio: municipios) {
            comboBox1.addItem(municipio.getNome());
        }

        comboBox1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.clear();

                List<Municipio> municipios = municipioModelDAO.Read(new BasicDBObject("nome", comboBox1.getSelectedItem().toString()), Municipio.class);

                if (municipios.size() != 0)
                {
                    Municipio municipioAtual = municipios.get(0);
                    nomeMunicipio.setText(municipioAtual.getNome());
                    issMunicipio.setText(String.valueOf(municipioAtual.getIss()));

                    for (Local local: municipioAtual.locais) {
                        model.add(0, local);
                        list1.setModel(model);
                    }
                    edit = true;
                }
                else
                {
                    edit = false;
                }
            }
        });


        adicionarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog dialog = new JDialog(myFrame, "Novo local");
                dialog.setContentPane(new CadastroLocal(dialog, new CallBackEvent<Local>() {
                    @Override
                    public void CallBack(Local element) {
                        model.add(0, element);
                        list1.setModel(model);
                    }
                }).telaLocal);
                dialog.pack();
                dialog.setVisible(true);

                System.out.println(dialog);
            }
        });


        removerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.remove(list1.getSelectedIndex());
            }
        });

        cadastrarMunicípioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!edit)
                {
                    Municipio newMunicipio = new Municipio(nomeMunicipio.getText(), Double.valueOf(issMunicipio.getText()));
                    for (int count = 0; count < model.size(); count++)
                    {
                        newMunicipio.AddLocal(model.get(count));
                    }
                    municipioModelDAO.Insert(newMunicipio);
                }
                else
                {
                    Municipio newMunicipio = new Municipio(nomeMunicipio.getText(), Double.valueOf(issMunicipio.getText()));
                    for (int count = 0; count < model.size(); count++)
                    {
                        newMunicipio.AddLocal(model.get(count));
                    }
                    municipioModelDAO.Update(new BasicDBObject("nome", comboBox1.getSelectedItem().toString()), newMunicipio);
                }
            }
        });
    }
}
