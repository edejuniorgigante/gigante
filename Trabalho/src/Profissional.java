import java.util.Date;

public class Profissional {
    @DataField(myType = DataType.ENUM, classType = TipoProfissional.class)
    private TipoProfissional tipo;
    private String nome;
    private String cpf;
    private String rg;
    private String pis;
    private String dataNascimento;
    private String ruaNum;
    private String bairro;
    private String cidade;
    private String cep;

    public Profissional() {
    }

    public Profissional(TipoProfissional tipo, String nome, String cpf, String rg, String pis, String dataNascimento, String ruaNum, String bairro, String cidade, String cep) {
        this.tipo = tipo;
        this.nome = nome;
        this.cpf = cpf;
        this.rg = rg;
        this.pis = pis;
        this.dataNascimento = dataNascimento;
        this.ruaNum = ruaNum;

        this.bairro = bairro;
        this.cidade = cidade;
        this.cep = cep;
    }

    public TipoProfissional getTipo() {
        return tipo;
    }

    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public String getRg() {
        return rg;
    }

    public String getPis() {
        return pis;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public String getRuaNum() {
        return ruaNum;
    }

    public String getBairro() {
        return bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public String getCep() {
        return cep;
    }
}
