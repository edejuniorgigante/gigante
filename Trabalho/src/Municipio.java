import java.util.ArrayList;
import java.util.List;

public class Municipio {
    private String nome;
    private double iss;
    @DataListField(myType = DataType.CLASS, classType = Local.class)
    public List<Local> locais;

    public Municipio()
    {
        locais = new ArrayList<>();
    }

    public Municipio(String nome, double iss) {
        this.nome = nome;
        this.iss = iss;
        locais = new ArrayList<>();
    }

    public void CadastrarLocal(Local local){
        locais.add(local);
    }

    public String getNome() {
        return nome;
    }

    public double getIss() {
        return iss;
    }

    public List<Local> getLocais() {
        return locais;
    }

    public void AddLocal(Local local){ locais.add(local); }

    @Override
    public String toString() {
        return nome;
    }

}