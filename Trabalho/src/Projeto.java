import java.util.List;

public class Projeto {

    protected int numProj;
    protected String nome;
    protected Municipio municipio;
    protected int minParticipantes;
    protected int maxParticipantes;
    protected List<Profissional> profissionais;
    protected EstadoProjeto estado;

    public Projeto(){ }

    public Projeto(int numProj, String nome, Municipio municipio, int minParticipantes, int maxParticipantes, List<Profissional> profissionais) {
        this.numProj = numProj;
        this.nome = nome;
        this.municipio = municipio;
        this.minParticipantes = minParticipantes;
        this.maxParticipantes = maxParticipantes;
        this.profissionais = profissionais;
    }
}
