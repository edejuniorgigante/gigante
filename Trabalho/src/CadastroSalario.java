import com.mongodb.BasicDBObject;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class CadastroSalario {
    public JPanel panel1;
    private JComboBox tipoSalario;
    private JTextField valorSalario;
    private JButton enviarSalario;

    boolean edit = false;

    public CadastroSalario()
    {
        for (TipoProfissional item : TipoProfissional.values()) {
            tipoSalario.addItem(item.toString());
        }

        ModelDAO<Salario> salarioModelDAO = new ModelDAO(Salario.class);
        List<Salario> result = salarioModelDAO.Read(new BasicDBObject("tipoProfissional", tipoSalario.getSelectedItem().toString()), Salario.class);
        if(result.size() != 0)
        {
            Salario salario = result.get(0);
            valorSalario.setText(String.valueOf(salario.getValor()));
            edit = true;
        }
        else
        {
            edit = false;
        }

        tipoSalario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Salario> result = salarioModelDAO.Read(new BasicDBObject("tipoProfissional", tipoSalario.getSelectedItem().toString()), Salario.class);
                if(result.size() != 0)
                {
                    Salario salario = result.get(0);
                    valorSalario.setText(String.valueOf(salario.getValor()));
                    edit = true;
                }
                else
                {
                    valorSalario.setText("");
                    edit = false;
                }
            }
        });

        enviarSalario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!edit) {
                    salarioModelDAO.Insert(new Salario(Enum.valueOf(TipoProfissional.class, tipoSalario.getSelectedItem().toString()), Double.valueOf(valorSalario.getText())));
                }
                else {
                    salarioModelDAO.Update(new BasicDBObject("tipoProfissional", tipoSalario.getSelectedItem().toString()),
                                           new Salario(Enum.valueOf(TipoProfissional.class, tipoSalario.getSelectedItem().toString()), Double.valueOf(valorSalario.getText())));
                }
            }
        });
    }
}
